from src import archive
import pandas as pd
import random


def test_correct_output_sma():
    array = [random.uniform(-10, 10) for i in range(100)]

    pd_array = pd.DataFrame(array)
    expected = pd_array.rolling(3, min_periods=1).mean()[0].to_list()
    expected = [float("{:.5f}".format(item)) for item in expected]

    result = archive.sma_filter(array, 3)
    result = [float("{:.5f}".format(item)) for item in result]
    assert result == expected


def test_correct_output_ema():
    array = [random.uniform(-10, 10) for i in range(100)]

    pd_array = pd.DataFrame(array)
    expected = pd.DataFrame.ewm(pd_array[0], span=3, adjust=False).mean().to_list()
    expected = [float("{:.5f}".format(item)) for item in expected]

    result = archive.ema_filter(array, 3)
    result = [float("{:.5f}".format(item)) for item in result]
    assert result == expected


def test_correct_output_smm():
    array = [random.uniform(-10, 10) for i in range(100)]

    pd_array = pd.DataFrame(array)
    expected = pd_array.rolling(3, min_periods=1).median()[0].to_list()
    expected = [float("{:.5f}".format(item)) for item in expected]

    result = archive.smm_filter(array, 3)
    result = [float("{:.5f}".format(item)) for item in result]
    assert result == expected
